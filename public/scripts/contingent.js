"use strict";

window.onload = function() {


	window.addEventListener('online',  function(){
		console.log("online")
		let alert = document.querySelector(".offline-alert");
		alert.classList.remove("open");
		alert.classList.add("close");
		setTimeout(() => alert.remove(), 1000);
	});

  window.addEventListener('offline', function(){
  		console.log("offline")
  		let alertHtml = '<div class="offline-alert alert marginTop warning-block" role="alert">'+
  		'<div><img src="/images/warning.svg" class="img" /></div>'+
  		'<div class="alert__container">Нет подключения к интернету</div>'+
  		'</div>';

  		document.querySelector(".container").insertAdjacentHTML( 'beforeend', alertHtml );
  		document.querySelector(".offline-alert").classList.add("open");
  });

  
    this.document.querySelector(".container").addEventListener("keydown", function(event){
       	// Обработка чисел у текстового инпута (разрешено только двузначное значение)
        if( event.target.classList.value.indexOf("count-students") !== -1 ){
        	// console.log(event.key)
        	if( !Number.isInteger(parseInt(event.key)) && event.key !== "Backspace" && event.key !== "Delete")
        		event.preventDefault()
        }
    });
}



































function checkCorrectContingent(){

	if( document.querySelector(".alert").classList.value.indexOf("open") != -1){
			document.querySelector(".alert").classList.add("close");
			document.querySelector(".alert").classList.remove("open");
	}

	document.querySelector(".container__dashboard__block-groups__nav .js-groups-tab").classList.remove("js-incorrect-block");
	document.querySelector(".container__dashboard__block-groups__nav .js-choice-tab").classList.remove("js-incorrect-block");

	let groups = document.querySelectorAll(".container__dashboard .container__dashboard__block-groups #groups-block .group");
	let groupCorrect = true;

	for (let i = 0; i < groups.length; i++) {

			let group = groups[i];
			let contingetGroup = group.querySelectorAll(".group__contingent .group__contingent__count-block");

			let budgetCount = contingetGroup[0].children[1].value;
			let vnebudgetCount = contingetGroup[1].children[1].value;
			let fbudgetCount = contingetGroup[2].children[1].value;
			let fvnebudgetCount = contingetGroup[3].children[1].value;

			let splits = group.querySelectorAll(".group__subgroups .group__subgroups__block");

			for (let y = 0; y < splits.length; y++) {
					let split = splits[y];
					let IdSplit = split.id;

					let sumBudgetCount = 0;
					let sumVnebudgetCount = 0;
					let sumFBudgetCount = 0;
					let sumFVnebudgetCount = 0;

					let subgroups = split.querySelectorAll(".group__subgroups__block__content .group__subgroups__block__content__value");
					for(let z = 0; z < subgroups.length; z++){
						let contingentSubgroup = subgroups[z].querySelectorAll(".js-contingent-subgroup");

						if( contingentSubgroup.length === 4 ){
							sumBudgetCount += parseInt(contingentSubgroup[0].children[1].value);
							sumVnebudgetCount += parseInt(contingentSubgroup[1].children[1].value);
							sumFBudgetCount += parseInt(contingentSubgroup[2].children[1].value);
							sumFVnebudgetCount += parseInt(contingentSubgroup[3].children[1].value);
						}
					}

					if( budgetCount != sumBudgetCount || vnebudgetCount != sumVnebudgetCount || fbudgetCount != sumFBudgetCount || fvnebudgetCount != sumFVnebudgetCount ){
						split.classList.add("invalid-block");
						groupCorrect = false;
						document.querySelector(".container__dashboard__block-groups__nav .js-groups-tab").classList.add("js-incorrect-block");
					}
					else
						split.classList.remove("invalid-block");
			}
	}


	let commonContingent = document.querySelector(".container__dashboard__common-contingent").children;
	
	let choiceBlock = document.querySelectorAll(".choice-block");
	let choiceGroupCorrect = true;
	
	for( let i=0; i< choiceBlock.length; i++ ){

		let sumBudgetCount = 0;
		let sumVnebudgetCount = 0;
		let sumFBudgetCount = 0; 
		let sumFVnebudgetCount = 0;

		let choiceContingent = choiceBlock[i].querySelectorAll(".choice-block__content__contingent__list");

		for (let y = 0; y < choiceContingent.length; y++) {
			sumBudgetCount += parseInt(choiceContingent[y].children[0].children[1].value)
			sumVnebudgetCount += parseInt(choiceContingent[y].children[1].children[1].value)
			sumFBudgetCount += parseInt(choiceContingent[y].children[2].children[1].value)
			sumFVnebudgetCount += parseInt(choiceContingent[y].children[3].children[1].value)
		}

		if( commonContingent[0].children[1].innerText != sumBudgetCount || commonContingent[1].children[1].innerText != sumVnebudgetCount ||
					commonContingent[2].children[1].innerText != sumFBudgetCount ||  commonContingent[3].children[1].innerText != sumFVnebudgetCount){
			choiceGroupCorrect = false;
			document.querySelector(".container__dashboard__block-groups__nav .js-choice-tab").classList.add("js-incorrect-block");
			choiceBlock[i].classList.add("invalid-block");
		}
		else
			choiceBlock[i].classList.remove("invalid-block");
	}

	if( !groupCorrect || !choiceGroupCorrect ){
		document.querySelector(".alert .alert__container__content").innerHTML = "<span class='text'>Проверьте количество студентов по подгруппам и курсам по выбору.</span>";
		document.querySelector(".alert").classList.remove("close");
		document.querySelector(".alert").classList.add("open");
	}
}