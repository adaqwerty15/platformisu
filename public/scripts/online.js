"use strict";

window.onload = function() {
	window.addEventListener('online',  function(){
		console.log("online")
		let alert = document.querySelector(".offline-alert");
		alert.classList.remove("open");
		alert.classList.add("close");
		setTimeout(() => alert.remove(), 1000);
	});

  window.addEventListener('offline', function(){
  		console.log("offline")
  		let alertHtml = '<div class="offline-alert alert marginTop warning-block" role="alert">'+
  		'<div><img src="/images/warning.svg" class="img" /></div>'+
  		'<div class="alert__container">Нет подключения к интернету</div>'+
  		'</div>';

  		document.querySelector(".container").insertAdjacentHTML( 'beforeend', alertHtml );
  		document.querySelector(".offline-alert").classList.add("open");
  });
}