"use strict";
window.onload = function() {
    window.addEventListener('online',  function(){
        console.log("online")
        let alert = document.querySelector(".offline-alert");
        alert.classList.remove("open");
        alert.classList.add("close");
        setTimeout(() => alert.remove(), 1000);
    });

  window.addEventListener('offline', function(){
        console.log("offline")
        let alertHtml = '<div class="offline-alert alert marginTop warning-block" role="alert">'+
        '<div><img src="/images/warning.svg" class="img" /></div>'+
        '<div class="alert__container">Нет подключения к интернету</div>'+
        '</div>';

        document.querySelector(".container").insertAdjacentHTML( 'beforeend', alertHtml );
        document.querySelector(".offline-alert").classList.add("open");
  });

  
	this.document.querySelector(".container").addEventListener("click", function(event){

        if( event.target.classList.value === "container__background" )
            event.target.remove();
        else{
            //opem dropdown menu
            let elem = (event.target.classList.value).indexOf("open-menu")
            let elemParent = (event.target.parentNode.classList.value).indexOf("open-menu")
            if( elem !== -1 || elemParent !== -1){

                let menu = (elem !== -1) ? event.target : event.target.parentNode;
                menu.parentNode.nextSibling.nextElementSibling.classList.toggle("open"); 
                menu.children[1].classList.toggle("rotated");

                let coord = (menu).getBoundingClientRect();
                menu.parentNode.nextSibling.nextElementSibling.style.width = coord.width+"px";
                menu.parentNode.nextSibling.nextElementSibling.style.top = (coord.y + coord.height) + "px";
            }
            else{

                // li
                if( (event.target.classList.value).indexOf("item-menu") !== -1 ){
                    let val = event.target.dataset.val

                    let elem = event.target.parentNode.previousSibling.previousElementSibling.children[1].children[0]
                    elem.dataset.elem = val;
                    elem.innerText = event.target.innerText;

                    let selectedItem = event.target.parentNode.querySelectorAll("li[data-selected='true']");

                    if( selectedItem.length == 1 )
                        selectedItem[0].dataset.selected = "false";
                    event.target.dataset.selected = "true";
                }
            }
        }
                 
    });
}