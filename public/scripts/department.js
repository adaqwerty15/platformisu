"use strict";
var top_scroll = 0;
window.onload = function() {
    this.document.querySelector(".container").addEventListener("keydown", function(event){
       	// Обработка чисел у текстового инпута (разрешено только двузначное значение)
        if( event.target.classList.value.indexOf("count-students") !== -1 ){
        	// console.log(event.key)
        	if( !Number.isInteger(parseInt(event.key)) && event.key !== "Backspace" && event.key !== "Delete")
        		event.preventDefault()
        }
    });


    // this.document.querySelector(".container__dashboard__left-block").addEventListener("scroll", function(e){
    // 	if( this.scrollTop != 0 ){
    // 		this.classList.add("scroll-table");
    // 	}
    // 	else{
    // 		this.classList.add("scroll-table");
    // 	}
    // });
}



function checkHoursTeacher(...teacherIds){
    let listTeachers = document.querySelectorAll(".container__dashboard__right-block__teachers .container__dashboard__right-block__teachers__item");
    listTeachers.forEach( e => {
    	if(teacherIds.includes(parseInt(e.dataset.teacher))){
    		let distributedHours = e.children[2].children[0].innerText;
    		let allHours = e.children[2].children[2].innerText;
    		
    		if( parseFloat(distributedHours) > parseFloat(allHours) )
    			e.classList.add("limit")
    		else
    			e.classList.remove("limit")
    	}
    });



}


function lineIsDistributed(uuid, groupId, groupType){

	let line = document.querySelectorAll("tbody .tr[data-uuid='"+uuid+"'][data-group='"+groupId+"'][data-groupType='"+groupType+"']")
	let distributedhours = parseFloat(line[0].dataset.distributedhours);
	let allhours = parseFloat(line[0].dataset.allhours);

	let className = "";
	if( distributedhours === allhours ){
		className = "add";
	}
	else{
		if( distributedhours === 0 )
			className = "remove";
	}

	let countTeacher = {}
	let teacher = "";

	line.forEach(e => {
		if( e.classList.value.indexOf("tr-subline") != -1 ){
			let el = e.lastChild.previousSibling.children[0];
			let id = el.dataset.id
			if(id.length > 0 ){
				countTeacher[id] = 1  + (countTeacher[id] || 0);
				teacher = el.innerText;
			}
		}

		if( className === "add" )
			e.classList.add("js-distributed");
		else{
			if( className === "remove")
				e.classList.remove("js-distributed");
		}
	});

	
	let len = Object.keys(countTeacher).length;
	
	if( len > 1 )
		line[0].lastChild.previousSibling.children[0].innerText = len + " преподавателя(ей)";
	else{
		if(len === 1)
			line[0].lastChild.previousSibling.children[0].innerText = teacher;
		else
			line[0].lastChild.previousSibling.children[0].innerText = "";
	}

	if(className === "remove")
		line[0].classList.remove("tr-distributed");
}
