"use strict"

window.onload = function() {

    window.addEventListener('online',  function(){
        console.log("online")
        let alert = document.querySelector(".offline-alert");
        alert.classList.remove("open");
        alert.classList.add("close");
        setTimeout(() => alert.remove(), 1000);
    });

     window.addEventListener('offline', function(){
        console.log("offline")
        let alertHtml = '<div class="offline-alert alert marginTop warning-block" role="alert">'+
        '<div><img src="/images/warning.svg" class="img" /></div>'+
        '<div class="alert__container">Нет подключения к интернету</div>'+
        '</div>';

        document.querySelector(".container").insertAdjacentHTML( 'beforeend', alertHtml );
        document.querySelector(".offline-alert").classList.add("open");
    });





    this.document.querySelector(".addFormulaForm .form__content__left-block").addEventListener('click',  function(e){
        let blockFormula = this.children[0].children[0];

        let classElem = e.target.classList.value;
        switch( classElem ){
            case "js-col-name text": 
                if( !blockFormula.children.length || blockFormula.lastChild.dataset.type === "js-operation" )
                    blockFormula.insertAdjacentHTML("beforeend", "<div class='row-formula-block' data-type='js-col-name' data-id="+e.target.dataset.id+"><span>"+e.target.innerText+" </span><div class='js-formula-remove'></div></div>");
                break;
            case "js-operation": 
                if( blockFormula.children.length && (blockFormula.lastChild.dataset.type === "js-col-name" || blockFormula.lastChild.dataset.type === "js-const" ))
                    blockFormula.insertAdjacentHTML("beforeend", "<div class='row-formula-block' data-type='js-operation'><span>"+e.target.innerText+" </span><div class='js-formula-remove'></div></div>");
                break;
            case "js-const": 
                let input = e.target.previousSibling;
                if(input){
                    if(input.validity.valid && input.value.substring(0, 1) !== "."){
                        if(!blockFormula.children.length || blockFormula.lastChild.dataset.type === "js-operation")
                            blockFormula.insertAdjacentHTML("beforeend", "<div class='row-formula-block' data-type='js-const'><span>"+input.value+" </span><div class='js-formula-remove'></div></div>");
                    }
                }
                break;
            case "js-formula-remove": 
                let parentElem = e.target.parentNode;
                if(parentElem.nextSibling)
                    parentElem.nextSibling.remove();
                parentElem.remove();
                break;
        }
    });
    





























    // $(".addFormulaForm .form__content__left-block").on("click", ".js-col-name", (e) => {
    //     let formulaBlocks = $(".js-formula-block").children();
    //     if( $(".js-formula-block").children().length == 0 || $($(".js-formula-block").children()[ formulaBlocks.length - 1]).attr("data-type") === "js-operation" ){
    //         $(".js-formula-block").append( "<div class='row-formula-block' data-type='js-col-name' data-id="+$(e.target).attr("data-id")+"><span>"+$(e.target).text()+" </span><div class='js-formula-remove'></div></div>")
    //     }

    // });
    // $(".addFormulaForm .form__content__left-block").on("click", ".js-operation", (e) => {
    //     let formulaBlocks = $(".js-formula-block").children();
    //     if( $($(".js-formula-block").children()[ formulaBlocks.length - 1]).attr("data-type") === "js-col-name" || $($(".js-formula-block").children()[ formulaBlocks.length - 1]).attr("data-type") === "js-blockk-add-const" ){
    //         $(".js-formula-block").append( "<div class='row-formula-block' data-type='js-operation'><span>"+$(e.target).text()+" </span><div class='js-formula-remove'></div></div>")
    //     }
    // });
    // $(".addFormulaForm .form__content__left-block").on("click", ".js-blockk-add-const", (e) => {
    //     let formulaBlocks = $(".js-formula-block").children();
    //     // console.log(  $(e.target).prev()[0].validity  )
    //     if($(e.target).prev()[0].validity.valid){
    //         if( $(".js-formula-block").children().length == 0 || $($(".js-formula-block").children()[ formulaBlocks.length - 1]).attr("data-type") === "js-operation" ){
    //             let firstSymb = "";
    //             if(( $($(e.target).prev()).val()[0] == ".")){
    //                 firstSymb = 0;
    //             }
    //             if( ($($(e.target).prev()).val()).trim().length !== 0){
    //                 $(".js-formula-block").append( "<div class='row-formula-block' data-type='js-blockk-add-const'><span>"+ (firstSymb + $($(e.target).prev()).val())+" </span><div class='js-formula-remove'></div></div>");
    //                 $($(e.target).prev()).val("");
    //             }
    //         }
    //     }
    // });


    // $(".addFormulaForm .form__content__left-block").on("click", ".js-formula-remove", (e) => {
    //     let blockFormula = $(e.target).parent();
    //     let nextElem = $(blockFormula).next();
    //     $( blockFormula ).remove() ;
    //     if( nextElem.length !==0 ){
    //         $(nextElem).remove();
    //     }
    // });
































    // $(".container").on("click", ".js-nav-item", (e) => {
    //     if( ! $( e.target ).hasClass("selected-item-formula-form") ){
    //         $(".js-nav-item").removeClass("selected-item-formula-form");
    //         $( e.target ).addClass("selected-item-formula-form");
    //         if( $(e.target).attr("data-name") === "formula" ){
    //             $(".js-create-formula-block").css({ "display": "block"});
    //             $(".js-formula-setting").css({ "display": "none"});
    //         } 
    //         else{
    //             $(".js-create-formula-block").css({ "display": "none"});
    //             $(".js-formula-setting").css({ "display": "block"});
    //         }
    //     }
    // });








    this.document.querySelector(".container__dashboard__right__cols").addEventListener("mouseout", function(event){
        if( event.relatedTarget !== null){

            let from = event.target.dataset.elem;
            let to = event.relatedTarget.dataset.elem;

            // console.log( "f -"+from);
            // console.log( to)

            if( from === "dots" && to !== "menu"){
                $( $( event.target ).next() ).css({ "display" : "none"})
            }
            else{
                if( from === "menu" && to !== "menu"){
                    $( event.target.parentNode ).css({ "display" : "none"})
                }
            }
        }
        else{
            // console.log("++")
            // if( from !== undefined && from === "menu"){
            //     $( event.target.parentNode ).css({ "display" : "none"})
            // }
        }
    });




    // $(".container").on( "keydown", ".js-formula-block",  function(event) {
           
    //         var x = event.charCode || event.keyCode;
            
            // if( x===8 || x===46 ){
            //     let ind = event.target.selectionStart
            //     let valInput = $(".js-formula-block").val();
                

            //     if( event.key === "Delete"){
            //         console.log("del")
            //         let c = (valInput.substring(ind, ind+1)).charCodeAt(0)
            //         console.log(  (c < 48 || c > 57) && c !== 43 && c !== 45 && c !== 42 && c !== 47 && c !== 46  )
            //         if( (c < 48 || c > 57) && c !== 43 && c !== 45 && c !== 42 && c !== 47 && c !== 46 ){
            //             console.log("+")
            //             event.preventDefault();
            //         }
            //     }
            //     else{
            //         console.log("back")
            //         let current = valInput.substring(ind-1, ind)
                    
            //         if( current.charAt(0) < 48 || current.chatAt(0) > 57 && current.charAt(0) !== 43 && current.charAt(0) !== 45 && current.charAt(0) !== 42 && current.charAt(0) !== 47 && current.charAt(0) !== 46){
            //             console.log("+")
            //             event.preventDefault();
            //         }
                    
            //     }


            // }
            
            // if( isNaN(String.fromCharCode(x)) ) {
            //     console.log(x)
            //     // + - / * . 
            //     if( x===56 || x===190 || x===191 || x===187 || x===189  || x===8 || x===46){
            //         let lastInputType = $(".js-formula-block").attr("data-lasttype");
                    
            //         if( x===190 ){
            //             if(  lastInputType ==="numbers"  )
            //                 $(".js-formula-block").attr("data-lasttype", "dot")
            //             else
            //                 event.preventDefault();
            //         }
            //         else{
            //             if( x===56 || x===191 || x===187 || x===189 ){
            //                 if(  lastInputType === "col-name" || lastInputType ==="numbers"  )
            //                     $(".js-formula-block").attr("data-lasttype", "operate")
            //                 else
            //                     event.preventDefault();
            //             }
            //             else{
            //                 if( x===8 || x===46 ){
            //                     console.log("remove")
            //                     console.log(event)
            //                 }
            //                 else
            //                     event.preventDefault();
            //             }
            //         }
            //     }
            //     else
            //         event.preventDefault();
            // }
            // else{
            //     // space
            //     if( x === 32 )
            //         event.preventDefault();
            //     else{
            //         //Ввод чисел
            //         if( x >= 48 && x <= 57 ){
            //             let lastInputType = $(".js-formula-block").attr("data-lasttype");
            //             if( lastInputType === "col-name" )
            //                 event.preventDefault();
            //             else
            //                 $(".js-formula-block").attr("data-lasttype", "numbers");
            //         }
            //     }
            // }
    // });
}