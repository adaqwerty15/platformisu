"use strict";
var left_scroll = 0;
var top_scroll = 0;

window.onload = function() {



    window.addEventListener('online',  function(){
        console.log("online")
        let alert = document.querySelector(".offline-alert");
        alert.classList.remove("open");
        alert.classList.add("close");
        setTimeout(() => alert.remove(), 1000);
    });

  window.addEventListener('offline', function(){
        console.log("offline")
        let alertHtml = '<div class="offline-alert alert marginTop warning-block" role="alert">'+
        '<div><img src="/images/warning.svg" class="img" /></div>'+
        '<div class="alert__container">Нет подключения к интернету</div>'+
        '</div>';

        document.querySelector(".container").insertAdjacentHTML( 'beforeend', alertHtml );
        document.querySelector(".offline-alert").classList.add("open");
  });

  

    this.document.querySelector(".container").addEventListener("keydown", function(event){
        // Обработка чисел у текстового инпута (разрешено только двузначное значение)
        if( event.target.classList.value.indexOf("count-students") !== -1 ){
            // console.log(event.key)
            if( !Number.isInteger(parseInt(event.key)) && event.key !== "Backspace" && event.key !== "Delete")
                event.preventDefault()
        }
    });


    this.document.querySelector(".menu-search__search-block input").addEventListener("input", function(event){
        // console.log( this.value )        
        let list = this.parentNode.nextSibling.nextElementSibling.children
        filterInput(this.value, list)

    });

    
    this.document.querySelector(".container").addEventListener("click", function(event){


        if( event.target.classList.value === "container__background" )
            event.target.remove();
        else{

            //open dropdown menu
            let elem = (event.target.classList.value).indexOf("open-menu")
            let elemParent = (event.target.parentNode.classList.value).indexOf("open-menu")
            if( elem !== -1 || elemParent !== -1){

                let menu = (elem !== -1) ? event.target : event.target.parentNode;
                menu.parentNode.nextSibling.nextElementSibling.classList.toggle("open"); 
                menu.children[1].classList.toggle("rotated");

                let coord = (menu).getBoundingClientRect();
                menu.parentNode.nextSibling.nextElementSibling.style.width = coord.width+"px";
                menu.parentNode.nextSibling.nextElementSibling.style.top = (coord.y + coord.height) + "px";
            }
            else{

                // li
                if( (event.target.classList.value).indexOf("item-menu") !== -1 ){
                    let val = event.target.dataset.val

                    let elem = event.target.parentNode.previousSibling.previousElementSibling.children[1].children[0]
                    elem.dataset.elem = val;
                    elem.innerText = event.target.innerText;

                    let selectedItem = event.target.parentNode.querySelectorAll("li[data-selected='true']");

                    if( selectedItem.length == 1 )
                        selectedItem[0].dataset.selected = "false";
                    event.target.dataset.selected = "true";
                }
            }
        }
                 
    });



    //Hint
    $(".container__table").on("mouseover", ".hint-name", (e) => {
        // console.log("hover")
        let elem = e.target
        let data_hint = "";
        if($(elem).hasClass("name-col"))
            data_hint = $($(elem).parent()).attr("data-hint");
        else
            data_hint = $(elem).attr("data-hint");

        

        if( data_hint.length > 0 ){
            let coord = ((elem).parentElement).getBoundingClientRect();

            $("body").append("<div class='js-hint-block'>"+data_hint+"</div>")
        
            let coordX = ((coord.left + (coord.width/2) - ($(".js-hint-block")[0].offsetWidth)/2));
            let coordY = coord.top - $(".js-hint-block")[0].offsetHeight;
            if( coordX < 0 )
                coordX = 10
            else{
                if( (coordX + ($(".js-hint-block")[0].offsetWidth)) > document.documentElement.clientWidth)
                    coordX = coordX - ((coordX + ($(".js-hint-block")[0].offsetWidth)) - document.documentElement.clientWidth)  
            }

            $(".js-hint-block").css({top: coordY, left: coordX})
        }
    });

    $(".container__table").on("mouseout", ".hint-name", (e) => {
        $(".js-hint-block").remove()
    });
    /////////


    this.document.querySelector(".container__dashboard__content__scroll").addEventListener("scroll", function(e){
        if( this.scrollLeft == 0){
            this.querySelectorAll(".js-horizontal-scroll").forEach( e => {
                e.classList.remove("js-aside-block-scrolling");
            });
        }
        else{
            if( this.scrollLeft !== left_scroll ){
                left_scroll = this.scrollLeft;
                alignment();
                this.querySelectorAll(".js-horizontal-scroll").forEach( e => {
                    e.classList.add("js-aside-block-scrolling");
                });
            }
        }
    });



    // $(".container__dashboard__content__scroll").on("scroll", (e) => {

    //     let step_left_scroll = $(".container__dashboard__content__scroll").scrollLeft();
    // //     let step_top_scroll = $(".container__dashboard__content__scroll").scrollTop();
    //     if(step_left_scroll == 0){
    //         $(".js-horizontal-scroll").removeClass("js-aside-block-scrolling");
    //         // $(".row-direction").css({"margin-left": 0})
    //         // $(".js-block-add-row").css({"margin-left": 0})
    //     }
    //     else{
    //         $(".js-horizontal-scroll").addClass("js-aside-block-scrolling");
    //         // $(".row-direction").css({"margin-left": (step_left_scroll+"px")})
    //         // $(".js-block-add-row").css({"margin-left": ((step_left_scroll.toFixed(2))+"px")})
    //     }

    // //     // console.log( step_top_scroll )
    // //     // down scroll
    // //     // if( this.top_scroll < step_top_scroll){
    // //     //     // console.log( this.document.getElementsByClassName("td-row-faculty"))
    // //     // }

    // //     // this.top_scroll = step_top_scroll;
    // });
    
}



window.onresize = function() {
    alignment();
};

function alignment(){

    //thead
    if( document.getElementsByClassName('tr-top').length !== 0){
        // let firstTrHeight = window.getComputedStyle(document.getElementsByClassName('tr-top')[0]).height;
        let firstTrHeight = (document.getElementsByClassName('tr-top')[0]).getBoundingClientRect().height;
        let secondTr = document.getElementsByClassName('tr-bottom')[0];
        let tdElems = secondTr.getElementsByTagName('td');
        for (let i = 0; i < tdElems.length; i++) {
            tdElems[i].style.top = (Math.ceil(firstTrHeight))+ "px";
            
        }
    }

    // direction
    if( document.getElementsByTagName('thead').length !== 0 ){
        let theadHeight = window.getComputedStyle(document.getElementsByTagName('thead')[0]).height;
        let faculties = document.getElementsByClassName('td-row-direction');
        for (let i = 0; i < faculties.length; i++) {
            faculties[i].style.top = theadHeight;
        }
    }


    // // second td
    let widthFirstTd = window.getComputedStyle( ((document.getElementsByTagName('tbody')[0]).querySelector('tr.line-load')).getElementsByTagName("td")[2] ).width;
    let tdSubject = document.getElementsByClassName('js-col-name-subject');
    widthFirstTd = (widthFirstTd.substr(0, widthFirstTd.indexOf("p")) - 0.5) + "px";
    for (let i = 0; i < tdSubject.length; i++) {
        tdSubject[i].style.left = widthFirstTd;
    }
    ((document.getElementsByTagName('thead')[0]).getElementsByTagName('tr')[0]).getElementsByTagName('td')[2].style.left = widthFirstTd;


    // let w = window.getComputedStyle(((document.getElementsByTagName('thead')[0]).getElementsByTagName('tr')[0]).getElementsByTagName('td')[0]).width;
    // $(".js-block-add-row").width(2 * parseFloat(w.substr(0, w.indexOf("p"))))
}





function filterInput(filter, list){
    for (let i = 0; i < list.length; i++) {
        let val = list[i].children[0].children[1].innerText;
        if( val.toLowerCase().indexOf(filter.toLowerCase()) == -1 ){
            //     list[i].style.display = "none";
            let sublist = list[i].children[1].children;
            let check = false;
            for(let y=0; y < sublist.length; y++){
              
                let elem = sublist[y].children[1].innerText;
                if( elem.toLowerCase().indexOf(filter.toLowerCase()) != -1 ){
                    check = true;
                }
            }
            if(!check)
                list[i].style.display = "none";
        }
        else
            list[i].style.display = "";
    }
}

