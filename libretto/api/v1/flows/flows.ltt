package api/flows

use libretto/io
use libretto/web/app
use libretto/db
use libretto/util
use com/teacode/string
use common/model as cm
use api/flows/model as afm
use api/load as al
use load
use api/load/model as alm
use api/groups/model as agm
use model/storage as ms

def db = ms/db
def getAllFlows(loadVersion: Int!) {

	var loadFlows: afm/Flow* = ()

	db.work(8):{
		fix queryl = db.prepare(<<select  res.fid, res.name, res.type, res.fname,  department, 
			main, res.uuid, res.group_type, res.group_id,
			subgroups.name as s, groups_profile.name as g, shifr, profile_name, sum(res.id) as id
			from 
			(select flows.type, flows.id as fid, flows.name as fname, ls.name, flows.department, flows.main, 
			 ls.uuid, ls.group_type, ls.group_id, load.profile_id, case when ls.id=main then ls.id else 0 end as id  from flows
			 left join loadstrokes as ls on flows.id=ls.flow_id
			 left join load on flows.load_version=load.version
			 where flows.load_version=? and (load.id=ls.load_id
			and flows.type=ls.hours_type or ls.uuid is null)) as res 
			left join groups_profile on res.group_id=groups_profile.id and res.group_type='profile'
			left join subgroups on res.group_id=subgroups.id and res.group_type='subgroup'
			left join profiles_full on res.profile_id=profiles_full.profile_id and res.group_type='choice'
			group by res.name, res.type, res.fid, res.fname,  department, main, res.uuid, res.group_type, res.group_id,
			s, g, shifr, profile_name>>)
		
		queryl.setInt(0, loadVersion)
		fix flows: Map#[Int!, afm/Flow!]! = map#[Int!,  afm/Flow!]()
		fix flowsParticipants: Map#[Int!, afm/FlowParticipant*]! = map#[Int!,  afm/FlowParticipant*]()


		queryl.query: {
			fix row = $
			fix id = row.int('fid).*dyn(#!)

			flows!id = afm/Flow(id, row.string('type).*dyn(#!), loadVersion, row.string('fname),  row.int('department).*dyn(#!), ())
			

			if (row.string('uuid)!=()) {
				var c = flowsParticipants!id 

				fix groupType = row.string('group_type).*dyn(#!)
				var groupName: String?
				fix main = if (row.int('main)==row.int('id)) unit else ()

				if (groupType=='choice)
					groupName = row.string('shifr)+ " "+row.string('profile_name)
				else if (groupType=='subgroup)
					groupName = row.string('s)
				else if (groupType=='profile)
					groupName = row.string('g)
				c += afm/FlowParticipant(row.string('uuid).*dyn(#!), main, row.string('name), row.int('group_id).*dyn(#!), groupType, groupName)
				flowsParticipants!id = c
			}
	
		}

		flows.keys as key {
			fix t = (flows!key).*dyn(#!)
			loadFlows += afm/Flow(t.id, t.type, loadVersion, t.name, t.department, flowsParticipants!key)
		}

		afm/Flows(loadFlows)
		
	}
}


def getAllFlowsWithoutParticipants(loadVersion: Int!) {

	var loadFlows: afm/Flow* = ()

	db.work(8):{
		fix queryl = db.prepare(<<select flows.type, flows.id as fid, flows.name as fname, 
		flows.department from flows where flows.load_version=? >>)
		queryl.setInt(0, loadVersion)


		queryl.query: {
			fix row = $
			fix id = row.int('fid).*dyn(#!)
			loadFlows += afm/Flow(id, row.string('type).*dyn(#!), loadVersion, row.string('fname), row.int('department).*dyn(#!), ())
		}

		afm/Flows(loadFlows)
		
	}
}

def createFlow(type: String!, loadVersion: Int!, name: String?, department: Int!) {
	db.work(8): {

		fix create = db.prepareKey(<<INSERT INTO flows(type, load_version, name, department) VALUES (?, ?, ?, ?);>>)
		create.setString(0, type)
		create.setInt(1, loadVersion)
		create.setString(2, name)
		create.setInt(3, department)

		fix id = create.updateKey()

		afm/Flow(id, type, loadVersion, name, department, ())
	}
}

def addFlowParticipant(flowId: Int!, uuid: String!, groupId: Int!, groupType: String!) {
	db.work(8): {

		fix flowInfo = db.prepare(<<select res.name, department, main, flow_id, count_hours, res.id, subgroups.name as s, 
			groups_profile.name as g, shifr, profile_name, res.year, load_version from 
			(select ls.name, flows.department, flows.main, ls.flow_id, ls.count_hours, 
			ls.id, group_type, group_id, load.profile_id, load.year, flows.load_version from flows, loadstrokes as ls, 
			load where flows.id=? and load.id=ls.load_id  and ls.uuid=?
			and ls.group_id=? and ls.group_type=? and flows.type=ls.hours_type and flows.load_version=load.version) as res 
			left join groups_profile on res.group_id=groups_profile.id and res.group_type='profile'
			left join subgroups on res.group_id=subgroups.id and res.group_type='subgroup'
			left join profiles_full on res.profile_id=profiles_full.profile_id and res.group_type='choice'>>)

		flowInfo.setInt(0, flowId)
		flowInfo.setString(1, uuid)
		flowInfo.setInt(2, groupId)
		flowInfo.setString(3, groupType)

		var groupName: String? 
		var loadVersion: Int! = 0 
		var year: Int! = 0
		var name: String?
		var department: Int?
		var main: Int?
		var count: Int! = 0
		var mainP: Int?
		var ids: Int* = ()
		var flow_id: Int! = -1

		flowInfo.query: {
			fix row = $
			if (groupType=='choice)
				groupName = row.string('shifr)+ " "+row.string('profile_name)
			else if (groupType=='subgroup)
				groupName = row.string('s)
			else if (groupType=='profile)
				groupName = row.string('g)

			name = row.string('name)
			department = row.int('department)
			loadVersion = row.int('load_version).*dyn(#!)
			year = row.int('year).*dyn(#!)
			main = row.int('main)
			fix count_h = row.int('count_hours).*dyn(#!)
			fix id = row.int('id)
			if (count_h > 0) {
				count = count_h
				mainP = id
			}
			if (row.int('flow_id)!=-1) flow_id = row.int('flow_id).*dyn(#!)
			ids+=id
		}

		if (mainP!=() and flow_id==-1) {

			if (main==()) {
				fix up = db.prepare(<<UPDATE flows set main=? WHERE id=?;>>)
				up.setInt(0, mainP)
				up.setInt(1, flowId)

				up.update()
			}

		
			ids as idstr. {
				var str: String! = <<UPDATE loadstrokes set department=?, flow_id=? WHERE id=?;>>
				if (main!=()) str = <<UPDATE loadstrokes set department=?, flow_id=?, count_hours=0 WHERE id=?;>>
				fix ups = db.prepare(str)
				ups.setInt(0, department)
				ups.setInt(1, flowId)
				ups.setInt(2, idstr)
				ups.update()
			}
		
			load/loadsReCalc(uuid, year, loadVersion)
		}


		if (mainP==()) afm/FlowAddedResult("error", (), (), ())
		else if (flow_id!=-1) afm/FlowAddedResult("already", getFlow(flow_id), (), ())
		else afm/FlowAddedResult("success", getFlow(flowId), afm/FlowParticipant(uuid, if (main==()) unit else (), name, groupId, groupType, groupName), al/getLoadStrokes(uuid, loadVersion, year))

		
	}
}

def addFlowParticipants(flowId: Int!, uuid: String!, groups: agm/UuidGroup*) {
	db.work(8): {

		var added: afm/FlowParticipant* = ()
		var notadded: afm/FailParticipant* = ()

		var department: Int?
		var flow_id: Int! = -1
		var loadVersion: Int! = 0 
		var year: Int! = 0
		var name: String?
		var main: Int?

		fix sel = db.prepare(<<select distinct loadstrokes.department as ld, flows.department 
			as fd from loadstrokes, flows, load where uuid=?
			and flows.load_version=load.version and loadstrokes.load_id=load.id and flows.id=?>>)
		sel.setInt(1, flowId)
		sel.setString(0, uuid)

		var allowDep: Unit? = unit

		sel.query: {
			if ($.int(0)!=$.int(1)) allowDep = ()
		}

		groups as gr {

			if (allowDep) {
				fix flowInfo = db.prepare(<<select res.name, department, main, flow_id, count_hours, res.id,  shifr, profile_name, res.year, load_version from 
				(select ls.name, flows.department, flows.main, ls.flow_id, ls.count_hours, 
				ls.id, group_type, group_id, load.profile_id, load.year, flows.load_version from flows, loadstrokes as ls, 
				load where flows.id=? and load.id=ls.load_id  and ls.uuid=?
				and ls.group_id=? and ls.group_type=? and flows.type=ls.hours_type and flows.load_version=load.version) as res 
				left join profiles_full on res.profile_id=profiles_full.profile_id and res.group_type='choice'>>)

				flowInfo.setInt(0, flowId)
				flowInfo.setString(1, uuid)
				flowInfo.setInt(2, gr.groupId)
				flowInfo.setString(3, gr.groupType)

				var groupName: String? 		
				var count: Int! = 0
				var mainP: Int?
				var ids: Int* = ()

				flowInfo.query: {
					fix row = $
					if (gr.groupType=='choice)
						groupName = row.string('shifr)+ " "+row.string('profile_name)
					else groupName = gr.groupName

					if (added==()) {
						name = row.string('name)
						department = row.int('department)
						loadVersion = row.int('load_version).*dyn(#!)
						year = row.int('year).*dyn(#!)
						main = row.int('main)
					}
					
					fix count_h = row.int('count_hours).*dyn(#!)
					fix id = row.int('id)
					if (count_h > 0) {
						count = count_h
						mainP = id
					}
					if (row.int('flow_id)!=-1) flow_id = row.int('flow_id).*dyn(#!)
					ids+=id
					}

					if (mainP!=() and flow_id==-1) {

				// fix updep = db.prepare(<<UPDATE loadstrokes set department=? WHERE uuid=? and load_id in (select id from load where version=?);>>)
				// updep.setInt(0, department)
				// updep.setString(1, uuid)
				// updep.update()

					if (main==()) {
						fix up = db.prepare(<<UPDATE flows set main=? WHERE id=?;>>)
						up.setInt(0, mainP)
						up.setInt(1, flowId)
						main = mainP
						up.update()
					}

				
					ids as idstr. {
						var str: String! = <<UPDATE loadstrokes set flow_id=? WHERE id=?;>>
						if (main!=idstr) str = <<UPDATE loadstrokes set  flow_id=?, count_hours=0 WHERE id=?;>>
						fix ups = db.prepare(str)
						ups.setInt(0, flowId)
						ups.setInt(1, idstr)
						ups.update()
					}
			
				}

				if (mainP==()) notadded += afm/FailParticipant(gr, "error")
				else if (flow_id!=-1) notadded += afm/FailParticipant(gr, "already")
				else added += afm/FlowParticipant(uuid, if (main==mainP) unit else (), name, gr.groupId, gr.groupType, groupName)

				load/loadsReCalc(uuid, year, loadVersion)
			}

			else notadded += afm/FailParticipant(gr, "department")

		}	
		
		afm/FlowAddedResultGroups(added, notadded, afm/StrokesGroupByUuid(uuid, al/getLoadStrokes(uuid, loadVersion, year)))
		
	}
}


def getFlow(flowId: Int!) {
	db.work(8):{
		fix queryl = db.prepare(<<select  res.name, res.type, res.load_version, res.fname,  department, 
			main, res.uuid, res.group_type, res.group_id,
			subgroups.name as s, groups_profile.name as g, shifr, profile_name, sum(res.id) as id
			from 
			(select flows.type, flows.load_version, flows.name as fname, ls.name, flows.department, flows.main, 
			 ls.uuid, ls.group_type, ls.group_id, load.profile_id, case when ls.id=main then ls.id else 0 end as id  from flows, 
			loadstrokes as ls, load where flows.id=? and load.id=ls.load_id  and ls.flow_id=?
			and flows.type=ls.hours_type and flows.load_version=load.version) as res 
			left join groups_profile on res.group_id=groups_profile.id and res.group_type='profile'
			left join subgroups on res.group_id=subgroups.id and res.group_type='subgroup'
			left join profiles_full on res.profile_id=profiles_full.profile_id and res.group_type='choice'
			group by res.name, res.type, res.load_version, res.fname,  department, main, res.uuid, res.group_type, res.group_id,
			s, g, shifr, profile_name>>)
		
		queryl.setInt(0, flowId)
		queryl.setInt(1, flowId)

		var flowParticipants: afm/FlowParticipant* = ()
		var type: String! = ""
		var loadVersion: Int! = 0
		var name: String? 
		var department: Int! = 0


		queryl.query: {
			fix row = $
			type = row.string('type).*dyn(#!)
			loadVersion = row.int('load_version).*dyn(#!)
			name = row.string('fname)
			department = row.int('department).*dyn(#!)

			fix groupType = row.string('group_type).*dyn(#!)
			var groupName: String?
			fix main = if (row.int('main)==row.int('id)) unit else ()

			if (groupType=='choice)
				groupName = row.string('shifr)+ " "+row.string('profile_name)
			else if (groupType=='subgroup)
				groupName = row.string('s)
			else if (groupType=='profile)
				groupName = row.string('g)
			flowParticipants += afm/FlowParticipant(row.string('uuid).*dyn(#!), main, row.string('name), row.int('group_id).*dyn(#!), groupType, groupName)
		}


		afm/Flow(flowId, type, loadVersion, name, department, flowParticipants)
	}
}

def deleteFlowParticipant(flowId: Int!, uuid: String!, groupId: Int!, groupType: String!) {
	db.work(8):{

		fix q = db.prepare(<<select * from (select main, type,  load_version, ls.id, ls.count_groups, lw.work_count, 
		CASE WHEN work_type=101 THEN 'lek' 
		when (work_type=103 or work_type=104) then 'pract'
		when  work_type=102 THEN 'lab' END as type_lw, year
		from flows, loadstrokes as ls, lessonplanstroke as lp, lessonplanworks as lw, loadversions as lv
		where flows.id=? and ls.uuid=? and group_type=? and group_id=?
		and flows.type=ls.hours_type and ls.flow_id=flows.id
		and ls.uuid=lp.uuid and lw.lessonplanstroke_id=lp.id
		and lv.id=load_version) as res
		where type=type_lw>>)

		q.setInt(0, flowId)
		q.setString(1, uuid)
		q.setInt(3, groupId)
		q.setString(2, groupType)

		var ids: Int* = ()
		var main: Int?
		var lw: Int! = 0
		var year: Int! = 0
		var count: Int?

		q.query: {
			fix row = $
			main = row.int('main)
			ids += row.int('id)
			count = row.int('work_count)
			lw = row.int('load_version).*dyn(#!)
			year = row.int('year).*dyn(#!)
		}

		if (not main in (ids)) {
			ids as id. {
				fix up = db.prepare(<<UPDATE loadstrokes set flow_id=? WHERE id=?;>>)
				up.setInt(0, -1)
				up.setInt(1, id)
				up.update()

				fix up2 = db.prepare(<<UPDATE loadstrokes set count_hours=? WHERE id=? and count_groups<>0;>>)
				up2.setInt(0, count)
				up2.setInt(1, id)
				up2.update()
			}

			load/loadsReCalc(uuid, year, lw)

			afm/StrokesGroupByUuid(uuid, al/getLoadStrokes(uuid, lw, year))		
			
		}

		else {
			cm/Message("error", "uuid is main in flow")
		}
	}
}

def changeMainInFlow(flowId: Int!, uuidNew: String!, groupIdNew: Int!, groupTypeNew: String!) {

	var res: afm/StrokesGroupByUuid* = ()

	db.work(8): {
		
		fix q = db.prepare(<<select * from (select main, type,  load_version, ls.id, ls.count_groups, lw.work_count, 
		CASE WHEN work_type=101 THEN 'lek' 
		when (work_type=103 or work_type=104) then 'pract'
		when  work_type=102 THEN 'lab' END as type_lw, year
		from flows, loadstrokes as ls, lessonplanstroke as lp, lessonplanworks as lw, loadversions as lv
		where flows.id=? and ls.uuid=? and group_type=? and group_id=?
		and flows.type=ls.hours_type and ls.flow_id=flows.id
		and ls.uuid=lp.uuid and lw.lessonplanstroke_id=lp.id
		and lv.id=load_version) as res
		where type=type_lw>>)

		q.setInt(0, flowId)
		q.setString(1, uuidNew)
		q.setInt(3, groupIdNew)
		q.setString(2, groupTypeNew)

		var lw: Int! = 0
		var year: Int! = 0
		var main: Int?
		var newMain: Int?
		var count: Int?

		q.query: {
			fix row = $
			main = row.int('main)

			lw = row.int('load_version).*dyn(#!)
			year = row.int('year).*dyn(#!)

			fix count_h = row.int('count_groups).*dyn(#!)
			fix id = row.int('id)
			if (count_h > 0) {
				count = row.int('work_count)
				newMain = row.int('id)
			}
		}


		if (newMain!=()) {

			fix sel = db.prepare(<<select uuid from loadstrokes where id=?>>)
			sel.setInt(0, main)

			var oldUuid: String! = ""

			sel.query: {
				oldUuid = $.string(0).*dyn(#!)
			}

			fix up2 = db.prepare(<<UPDATE loadstrokes set count_hours=? WHERE id=?;>>)
			up2.setInt(0, 0)
			up2.setInt(1, main)
			up2.update()

			fix up = db.prepare(<<UPDATE loadstrokes set count_hours=? WHERE id=?;>>)
			up.setInt(0, count)
			up.setInt(1, newMain)
			up.update()

			fix up3 = db.prepare(<<UPDATE flows set main=? WHERE id=?;>>)
			up3.setInt(0, newMain)
			up3.setInt(1, flowId)
			up3.update()

			if (oldUuid==uuidNew) {
				load/loadsReCalc(uuidNew, year, lw)
				res+= afm/StrokesGroupByUuid(uuidNew, al/getLoadStrokes(uuidNew, lw, year))
			}

			else {
				load/loadCalc(lw, year)
				res+= afm/StrokesGroupByUuid(uuidNew, al/getLoadStrokes(uuidNew, lw, year))
				res+= afm/StrokesGroupByUuid(oldUuid, al/getLoadStrokes(oldUuid, lw, year))
			}

			
		}

	}

	res		
}

def deleteFlow(flowId: Int!) {

	var ls: afm/StrokesGroupByUuid* = ()

	db.work(8): {
		fix q = db.prepare(<<select * from (select type,  ls.uuid, load_version, ls.id, ls.count_groups, lw.work_count, 
		CASE WHEN work_type=101 THEN 'lek' 
		when (work_type=103 or work_type=104) then 'pract'
		when  work_type=102 THEN 'lab' END as type_lw, year
		from flows, loadstrokes as ls, lessonplanstroke as lp, lessonplanworks as lw, loadversions as lv
		where flows.id=?
		and flows.type=ls.hours_type and ls.flow_id=flows.id
		and ls.uuid=lp.uuid and lw.lessonplanstroke_id=lp.id
		and lv.id=load_version) as res
		where type=type_lw>>)

		q.setInt(0, flowId)  

		var lw: Int! = 0
		var year: Int! = 0
		var uuids: String* = ()

		q.query: {
			fix row = $
			uuids += row.string('uuid).*dyn(#!)
			lw = row.int('load_version).*dyn(#!)
			year = row.int('year).*dyn(#!)

			fix id = row.int('id)
			fix count_h = row.int('count_groups).*dyn(#!)
			fix count = row.int('work_count)

			fix up = db.prepare(<<UPDATE loadstrokes set count_hours=?, flow_id=-1 WHERE id=?;>>)
			up.setInt(0, count*count_h)
			up.setInt(1, id)
			up.update()
		}

		fix up2 = db.prepare(<<delete from flows WHERE id=?;>>)
		up2.setInt(0, flowId)
		up2.update()

		load/loadCalc(lw, year)

		uuids as uuid.{
			ls += afm/StrokesGroupByUuid(uuid, al/getLoadStrokes(uuid, lw, year))
		}

		ls

	}	
}

def editFlow (flowId: Int!, name: String?, department: Int?) {

	//var ls: afm/StrokesGroupByUuid* = ()

	db.work(8): {
		fix up = db.prepare(<<UPDATE flows set name=? WHERE id=?;>>)
		up.setString(0, name)
		up.setInt(1, flowId)
		up.update()

		// if (department!=()) {

		// 	fix up2 = db.prepare(<<UPDATE loadstrokes set department=? WHERE flow_id=?;>>)
		// 	up2.setInt(0, department)
		// 	up2.setInt(1, flowId)
		// 	up2.update()

		// 	fix sel = db.prepare(<<select distinct uuid, year, version from loadstrokes, load where flow_id=? and loadstrokes.load_id=load.id>>)
		// 	sel.setInt(0, flowId)

		// 	sel.query: {
		// 		fix row = $
		// 		fix uuid = row.string('uuid).*dyn(#!)
		// 		ls += afm/StrokesGroupByUuid(uuid, al/getLoadStrokes(uuid, row.int('version).*dyn(#!), row.int('year).*dyn(#!)))
		// 	}
		// }
		
	}

	//ls
	
}