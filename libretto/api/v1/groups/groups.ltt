package api/groups

use libretto/io
use libretto/web/app
use libretto/db
use libretto/util
use com/teacode/string
use api/groups/model as agm
use api/load/model as alm
use api/load as al
use common/model as cm
use load
use model/storage as ms

def db = ms/db

def getGroupsForStroke(uuid: String!, loadVersion: Int!) {

	var groups: agm/UuidGroup* = ()

	db.work(8): {
		fix sel = db.prepare(<<select distinct ls.name, ls.group_id, ls.group_type, gp.name as gn from loadstrokes as ls, load, 
			groups_profile as gp  where ls.uuid=? and load.id=ls.load_id and
			load.version=? and group_type='profile' and gp.id=ls.group_id
			UNION
			select distinct ls.name, ls.group_id, ls.group_type, '' as gn from loadstrokes as ls, load  
			where ls.uuid=? and load.id=ls.load_id and
			load.version=? and group_type='choice' >>)

		sel.setString(0, uuid)
		sel.setInt(1, loadVersion)
		sel.setString(2, uuid)
		sel.setInt(3, loadVersion)
	
		var num: Int! = 1

		sel.query: {
			fix row = $
			fix type = row.string('group_type).*dyn(#!)
			if (type == 'choice) {
				groups += agm/UuidGroup(row.int('group_id).*dyn(#!), type, row.string('name).*dyn(#!) + " " + num)
				num = num + 1
			} 
			else
				groups += agm/UuidGroup(row.int('group_id).*dyn(#!), type, row.string('gn).*dyn(#!))

		}
	}

	agm/UuidGroups(groups)
}

def addProfileGroup(profileId: Int!, course: Int!, year: Int!, name: String!, budzhetCount: Int!, vnebudzhetCount: Int!, budzhetForeign: Int!, vnebudzhetForeign: Int!) {
	db.work(8):{
		fix budzhet = if (budzhetCount>=vnebudzhetCount) unit else ()
		fix insgroup = db.prepareKey(<<INSERT INTO groups_profile(
	name, budzhet_count, vnebudzhet_count, budzhet_foreign, vnebudzhet_foreign, budzhet, profile_id, course, year)
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);>>)
		insgroup.setString(0, name)
		insgroup.setInt(1, budzhetCount)
		insgroup.setInt(2, vnebudzhetCount)
		insgroup.setInt(3, budzhetForeign)
		insgroup.setInt(4, vnebudzhetForeign)
		insgroup.setUnit(5, budzhet)
		insgroup.setInt(6, profileId)
		insgroup.setInt(7, course)
		insgroup.setInt(8, year)
		fix id = insgroup.updateKey()

		var lpId: Int! = -1

		fix lp = db.prepare(<<select id from lessonplan where course=? and profile_id=? and year=?>>)
		lp.setInt(1, profileId)
		lp.setInt(0, course)
		lp.setInt(2, year)

		lp.query: {
			lpId = $.int(0).*dyn(#!)
		}

		if (lpId!=-1) {

			fix addCont = db.prepare(<<select
			case when
			(select count(groups_choice.uuid) from groups_choice, lessonplanstroke
			where groups_choice.uuid=lessonplanstroke.uuid
			and lessonplanstroke.lessonplan_id=?)=0 then 
			(select count(lessonplanstroke) from lessonplanstroke
			where choice=true
			and lessonplanstroke.lessonplan_id=?)
			else -1 end;>>)

			addCont.setInt(0, lpId)
			addCont.setInt(1, lpId)

			addCont.query: {

				fix count = $.int(0)

				if (count>0) {
					fix choicePlan = db.prepare("select  uuid from lessonplanstroke where lessonplan_id=? and choice=true order by sem, code;")
					choicePlan.setInt(0, lpId)	

					var ch = 0
					choicePlan.query: {
						fix uuid = $.string(0)
						fix insert = db.prepareKey(<<insert into groups_choice(
						uuid, budzhet_count, vnebudzhet_count, budzhet_foreign, vnebudzhet_foreign, block)
						VALUES (?, ?, ?, ?, ?, ?);>>)

						insert.setString(0, uuid)
						if (ch mod 2 == 0) {
							insert.setInt(1, budzhetCount)
							insert.setInt(2, vnebudzhetCount)
							insert.setInt(3, budzhetForeign)
							insert.setInt(4, vnebudzhetForeign)
							insert.setString(5, lpId.string/string+"_"+ch)
						}
						else  {
							insert.setInt(1, 0)
							insert.setInt(2, 0)
							insert.setInt(3, 0)
							insert.setInt(4, 0)
							insert.setString(5, lpId.string/string+"_"+(ch-1))
						}
						

						ch = ch+1

						fix ins = insert.updateKey()

					}
				}

				else if (count == -1) {
					fix choiceGr = db.prepare(<<select gc.id, budzhet_count, vnebudzhet_count, 
						budzhet_foreign, vnebudzhet_foreign
						from groups_choice as gc, lessonplanstroke
						where gc.uuid=lessonplanstroke.uuid and
						lessonplan_id=?
						order by block, id;>>)

					choiceGr.setInt(0, lpId)

					var ch: Int! = 0
					choiceGr.query: {
						fix row = $
						if (ch mod 2 == 0) {
							fix up = db.prepareKey(<<update groups_choice set budzhet_count=?, vnebudzhet_count=?, 
							budzhet_foreign=?, vnebudzhet_foreign=? where id=? >>)
							up.setInt(0, row.int('budzhet_count) + budzhetCount)
							up.setInt(1, row.int('vnebudzhet_count) + vnebudzhetCount)
							up.setInt(2, row.int('budzhet_foreign) + budzhetForeign)
							up.setInt(3, row.int('vnebudzhet_foreign) + vnebudzhetForeign)
							up.setInt(4, row.int('id))
							fix k = up.updateKey()
						}
						ch = ch + 1
					}

				}
			}
		}

		agm/ProfileGroup(id, profileId, course, year, name, budzhetCount, vnebudzhetCount, budzhetForeign, vnebudzhetForeign)
	}
}

def deleteProfileGroup(groupId: Int!) {
	db.work(8):{

		fix countGr = db.prepare(<<select count(gp.id), gp.course, gp.year, gp.profile_id 
			from groups_profile as gp, groups_profile as gp2 where gp.id=?
			and gp.year=gp2.year and gp.profile_id=gp2.profile_id and gp.course=gp2.course
			group by gp.course, gp.year, gp.profile_id >>)

		countGr.setInt(0, groupId)

		countGr.query: {
			fix row = $
			if (row.int(0)==1) {
				fix delc = db.prepare(<<delete from groups_choice where id in (select gc.id from groups_choice as gc, 
					lessonplanstroke, lessonplan
					where gc.uuid=lessonplanstroke.uuid
					and lessonplan.id=lessonplanstroke.lessonplan_id
					and year=?
					and course=?
					and profile_id=?);>>)
				delc.setInt(0, row.int('year))
				delc.setInt(1, row.int('course))
				delc.setInt(2, row.int('profile_id))
				delc.update()
			}
		}	

		fix delgroup = db.prepare(<<delete from groups_profile where id=?;>>)
		delgroup.setInt(0, groupId)
		delgroup.update()

		fix delgroupS = db.prepare(<<delete from subgroups where parent_id=?;>>)
		delgroupS.setInt(0, groupId)
		delgroupS.update()


		cm/Message("ok", "sucsess deleted")
	}
}

def editProfileGroupName(groupId: Int!, newName: String!) {
	db.work(8):{
		fix upgroup = db.prepareKey(<<update groups_profile set name=? where id=?;>>)
		upgroup.setInt(1, groupId)
		upgroup.setString(0, newName)
		fix k = upgroup.updateKey()
		getProfileGroup(groupId)
	}
}


def editProfileGroupContingent(groupId: Int!, budzhetCount: Int!, vnebudzhetCount: Int!, budzhetForeign: Int!, vnebudzhetForeign: Int!) {
	db.work(8):{
		fix budzhet = if (budzhetCount>=vnebudzhetCount) unit else ()
		fix upgroup = db.prepareKey(<<update groups_profile set budzhet_count=?, vnebudzhet_count=?, budzhet_foreign=?, vnebudzhet_foreign=?, budzhet=? where id=?;>>)
		upgroup.setInt(5, groupId)
		upgroup.setInt(0, budzhetCount)
		upgroup.setInt(1, vnebudzhetCount)
		upgroup.setInt(2, budzhetForeign)
		upgroup.setInt(3, vnebudzhetForeign)
		upgroup.setUnit(4, budzhet)
		fix k = upgroup.updateKey()
		getProfileGroup(groupId)
	}
}


def getProfileGroup(groupId: Int!) {
	var res: agm/ProfileGroup? = ()

	db.work(8): {
		fix querysel = db.prepare(<<SELECT name, budzhet_count, vnebudzhet_count, budzhet_foreign, vnebudzhet_foreign, budzhet, profile_id, course, year from groups_profile where id=?;>>)
		querysel.setInt(0, groupId)
		querysel.query:{
			res = agm/ProfileGroup(groupId, $.int('profile_id).*dyn(#!), $.int('course).*dyn(#!), $.int('year).*dyn(#!), $.string('name).*dyn(#!), $.int('budzhet_count).*dyn(#!),  $.int('vnebudzhet_count).*dyn(#!), $.int('budzhet_foreign).*dyn(#!), $.int('vnebudzhet_foreign).*dyn(#!))
		}
	}		
	res
}


def getSubgroup(groupId: Int!) {
	var res: agm/Subgroup? = ()

	db.work(8): {
		fix querysel = db.prepare(<<SELECT name, budzhet_count, vnebudzhet_count, budzhet_foreign, vnebudzhet_foreign, parent_id, block_id from subgroups where id=?;>>)
		querysel.setInt(0, groupId)
		querysel.query:{
			res = agm/Subgroup(groupId, $.string('name).*dyn(#!), $.int('parent_id).*dyn(#!), $.int('block_id).*dyn(#!), $.int('budzhet_count).*dyn(#!), $.int('vnebudzhet_count).*dyn(#!), $.int('budzhet_foreign).*dyn(#!), $.int('vnebudzhet_foreign).*dyn(#!))
		}
	}		
	res
}

def addSubgroupsBlock(groupsBlock: agm/SubgroupAdd*) {
	var resGroups: agm/Subgroup* = ()

	db.work(8): {
		var block_id: Int! = 0
		fix countsel = db.prepare(<<SELECT max(block_id) from subgroups;>>)
		countsel.query:{
			 if ($.int(0)!=()) block_id = $.int(0).*dyn(#!)+1
		}
		println: block_id
		groupsBlock as gb {
			fix insgroup = db.prepareKey(<<INSERT INTO subgroups(
				name, parent_id, block_id, budzhet_count, vnebudzhet_count, budzhet_foreign, vnebudzhet_foreign, active)
				VALUES (?, ?, ?, ?, ?, ?, ?, ?);>>)
			insgroup.setString(0, gb.name)
			insgroup.setInt(3, gb.budzhetCount)
			insgroup.setInt(4, gb.vnebudzhetCount)
			insgroup.setInt(5, gb.budzhetForeign)
			insgroup.setInt(6, gb.vnebudzhetForeign)
			insgroup.setInt(1, gb.parentGroupId)
			insgroup.setInt(2, block_id)
			insgroup.setUnit(7, unit)
		
			fix id = insgroup.updateKey()
			println: id
			resGroups+=agm/Subgroup(id, gb.name, gb.parentGroupId, block_id, gb.budzhetCount, gb.vnebudzhetCount, gb.budzhetForeign, gb.vnebudzhetForeign)
		}
	}

	resGroups
}

def editSubgroupContingent(groupId: Int!, budzhetCount: Int!, vnebudzhetCount: Int!, budzhetForeign: Int!, vnebudzhetForeign: Int!) {
	db.work(8):{
		fix upgroup = db.prepareKey(<<update subgroups set budzhet_count=?, vnebudzhet_count=?, budzhet_foreign=?, vnebudzhet_foreign=?  where id=?;>>)
		upgroup.setInt(4, groupId)
		upgroup.setInt(0, budzhetCount)
		upgroup.setInt(1, vnebudzhetCount)
		upgroup.setInt(2, budzhetForeign)
		upgroup.setInt(3, vnebudzhetForeign)
		fix k = upgroup.updateKey()
		getSubgroup(groupId)
	}
}

def editSubroupName(groupId: Int!, newName: String!) {
	db.work(8):{
		fix upgroup = db.prepareKey(<<update subgroups set name=? where id=?;>>)
		upgroup.setInt(1, groupId)
		upgroup.setString(0, newName)
		fix k = upgroup.updateKey()
		getSubgroup(groupId)
	}
}

def deleteSubgroupBlock(blockId: Int!) {
	db.work(8):{
		fix upgroup = db.prepareKey(<<update subgroups set active=false where block_id=?;>>)
		upgroup.setInt(0, blockId)
		fix k = upgroup.updateKey()
		cm/Message("ok", "sucsess deleted")
	}
}


def getStrokeGroupsInformation(uuid: String!, loadVersion: Int!) {
	db.work(8): {
		fix groups: Map#[Int!, agm/GrInf*]! = map#[Int!, agm/GrInf*]()
		fix allgroups: Map#[Int!, agm/StrokeBlock*]! = map#[Int!, agm/StrokeBlock*]()

		fix q = db.prepare(<<select * from (select distinct  ls.choice, 
		 ls.group_id,  gp.name as gname
		 from loadstrokes as ls, load, lessonplanstroke as lp, lessonplanworks as lw, groups_profile as gp
		 where ls.uuid=?
		 and group_type='profile'
		 and ls.load_id=load.id 
		 and gp.id=group_id
		 and lp.uuid=ls.uuid and lw.lessonplanstroke_id=lp.id
		 and load.version=?) as res
		 left join
		 (select distinct  ls.department, ls.hours_type,
		 ls.group_id as sid,  s.name as sname, s.block_id, s.parent_id
		 from loadstrokes as ls, load, lessonplanstroke as lp, lessonplanworks as lw, subgroups as s
		 where ls.uuid=?
		 and group_type='subgroup'
		 and ls.load_id=load.id 
 		 and s.id=group_id
		 and lp.uuid=ls.uuid and lw.lessonplanstroke_id=lp.id
		 and load.version=?) as res2
		 on res.group_id=res2.parent_id
		 order by group_id, block_id, sid
		 >>)

		q.setString(0, uuid)
		q.setString(2, uuid)
		q.setInt(1, loadVersion)
		q.setInt(3, loadVersion)

		q.query: {
			fix row = $
			fix id = row.int('group_id).*dyn(#!)
			var c = groups!id
			c += agm/GrInf(row.string('gname), row.int('department), row.int('sid), row.string('sname), row.int('block_id), row.string('hours_type))
			groups!id = c
		}

		 fix qAll = db.prepare(<<select * from (select distinct  ls.choice, ls.department,
		 ls.group_id,  gp.name as gname
		 from loadstrokes as ls, load, lessonplanstroke as lp, lessonplanworks as lw, groups_profile as gp
		 where ls.uuid=?
		 and group_type='profile'
		 and ls.load_id=load.id 
		 and gp.id=group_id
		 and lp.uuid=ls.uuid and lw.lessonplanstroke_id=lp.id
		 and load.version=?) as res
		 inner join
		 (select distinct name as sname, s.block_id, s.parent_id, s.id as sid from subgroups as s where s.active=true) as res2
		 on res.group_id=res2.parent_id
		 order by group_id, block_id, sid>>)

		 qAll.setString(0, uuid)
		 qAll.setInt(1, loadVersion)

		 var blId: Int? = ()
		 var sb: agm/StrokeSubgroup* = ()
		 var id: Int! = -1
		 var name: String! = ""

		 qAll.query: {
		 	fix row = $
			
			fix block = row.int('block_id).*dyn(#!)
			fix sname = row.string('sname)

			if (blId!=block) {
					if (blId!=()) {
						var c = allgroups!id
						c +=  agm/StrokeBlock(blId, name.string/substring(0, $.string/length-4) , sb)
						allgroups!id = c
					}
					sb = ()
					blId = block
					name = ""
			}

			name = name + sname.*dyn(#!) + " / "
			id = row.int('group_id).*dyn(#!)
			sb += agm/StrokeSubgroup(row.int('sid), sname, row.int('department))

		 }

		 var c = allgroups!id
		 c +=  agm/StrokeBlock(blId, name.string/substring(0, $.string/length-4) , sb)
		 allgroups!id = c
	

		var sg: agm/StrokeGroup* = ()

		groups.keys as key {
			//println: 
			var name: String! = ""
			var gname: String?
			var blId: Int? = ()
			var sub: agm/StrokeSubgroup* = ()

			var typesDivision: String* = ()

			groups!key as gr {
				println: gr
				gname = gr.gname
				if (gr.blockId!=()) {
					blId = gr.blockId
					
					if (gr.type != "other" and not (gr.type in typesDivision)) typesDivision+=gr.type
					if (sub.where($.id==gr.sId) == ()) {
						name = name + gr.sName.*dyn(#!) + " / "
						sub += agm/StrokeSubgroup(gr.sId, gr.sName, gr.department)
					}
					
				}
			}

			if (blId!=())
				sg += agm/StrokeGroup(key, gname, agm/StrokeBlock(blId, name.string/substring(0, $.string/length-4), sub), typesDivision, allgroups!key)
			else	
				sg += agm/StrokeGroup(key, gname, (), (), allgroups!key)
		}

		 agm/StrokeGroups(sg)
	}
}

def appointGroupsBlocks(uuid: String!, blocks: agm/StrokeGroupBlock*, loadVersion: Int!) {
	
	db.work(8): {

	var year: Int! = 0

	fix yql = db.prepare(<<SELECT distinct year from  load
		where load.version=? ;>>)

		yql.setInt(0, loadVersion)

		yql.query: {
			year = $.int(0).*dyn(#!)
		}

	fix qAll = db.prepare(<<select * from (select distinct  
		 ls.group_id
		 from loadstrokes as ls, load, lessonplanstroke as lp, lessonplanworks as lw, groups_profile as gp
		 where ls.uuid=?
		 and group_type='profile'
		 and ls.load_id=load.id 
		 and gp.id=group_id
		 and lp.uuid=ls.uuid and lw.lessonplanstroke_id=lp.id
		 and load.version=?) as res
		 inner join
		 (select distinct  
		   s.block_id, s.parent_id, ls.hours_type
		 from loadstrokes as ls, load, lessonplanstroke as lp, lessonplanworks as lw, subgroups as s
		 where ls.uuid=?
		 and group_type='subgroup'
		 and ls.load_id=load.id 
 		 and s.id=group_id
		 and lp.uuid=ls.uuid and lw.lessonplanstroke_id=lp.id
		 and load.version=?) as res2
		 on res.group_id=res2.parent_id>>)

	qAll.setString(0, uuid)
	qAll.setInt(1, loadVersion)
	qAll.setString(2, uuid)
	qAll.setInt(3, loadVersion)

	fix groups: Map#[Int!, Int!]! = map#[Int!, Int!]()
	fix groups_types: Map#[Int!, String*]! = map#[Int!, String*]()

	qAll.query: {
		fix row = $
		groups!(row.int('group_id).*dyn(#!)) = row.int('block_id).*dyn(#!)
		var c = groups_types!(row.int('group_id).*dyn(#!))
		fix type = row.string('hours_type)
		if (type!="other")
			c += type
		groups_types!(row.int('group_id).*dyn(#!)) = c	
	}

		
	blocks as block {	

		if (block.blockId!=groups!(block.groupId)) {
			if (groups!(block.groupId)==() and block.blockId!=()) {
				println: "new appoint"
				appointGroupsBlock(uuid, block.blockId, loadVersion, block.subDepartments, block.typesDivision)	
			}

			else if (groups!(block.groupId)!=() and block.blockId==()) {
				println: "new join"
				joinGroups(uuid, loadVersion, block.groupId)
			}

			else if (groups!(block.groupId)!=() and block.blockId!=()) {
				println: "new change"
				joinGroups(uuid, loadVersion, block.groupId)
				appointGroupsBlock(uuid, block.blockId, loadVersion, block.subDepartments, block.typesDivision)
			}

			load/loadsReCalc(uuid, year, loadVersion)
		}

		else if (block.blockId==groups!(block.groupId)) {
			if (block.subDepartments!=()) {
				block.subDepartments as sd {
				fix up = db.prepareKey(<<update loadstrokes set department=? where id in
				 (SELECT loadstrokes.id from loadstrokes, load
				where loadstrokes.load_id=load.id and load.version=?
				 and uuid=? and active=true and group_type='subgroup' and group_id=?)>>)

				up.setInt(0, sd.department)
				up.setInt(1, loadVersion)
				up.setString(2, uuid)
				up.setInt(3, sd.subgroupId)

				fix k = up.updateKey()
				}
			}

			println: groups_types!(block.groupId)
			println: block.typesDivision

			if (block.typesDivision!=())
			{
				if (block.typesDivision.*size!=groups_types!(block.groupId).*size) {
					joinGroups(uuid, loadVersion, block.groupId)
					appointGroupsBlock(uuid, block.blockId, loadVersion, block.subDepartments, block.typesDivision)
					load/loadsReCalc(uuid, year, loadVersion)
				}

				else {
					var c: Unit? = ()
					block.typesDivision as b{
						if (not b in groups_types!(block.groupId)) c = unit 
					}

					if (c==unit) {
						joinGroups(uuid, loadVersion, block.groupId)
						appointGroupsBlock(uuid, block.blockId, loadVersion, block.subDepartments, block.typesDivision)
						load/loadsReCalc(uuid, year, loadVersion)
					}
				}

			}
			
		}

		

	}
		
		//println:al/getLoadStrokes(uuid, loadVersion, year)
		al/getLoadStrokes(uuid, loadVersion, year)

	}

	

}


def appointGroupsBlock(uuid: String!, blockId: Int?, loadVersion: Int!, subDepartments: agm/SubgroupDepartment*, typesDivision: String*) {
	var year:Int! = 0

	var stq:String! = ""

	if (typesDivision == ()) {
		stq = "('other')"
	}
	else {
		typesDivision as t.{
			stq = stq + "'" + t + "', "
		}
		stq = stq.string/substring(0, $.string/length-3)
		stq = "(" + stq + ")"
	}

	println: stq
	
	fix qload = db.prepare(s<<select ls.id, ls.load_id, ls.code, ls.name, ls.sem, ls.budzhet,
		 ls.department,  ls.choice, ls.count_students, ls.count_groups, ls.teacher_id, 
		 ls.group_id, ls.alias_name, ls.count_hours, ls.hours_type, ls.is_own, 
		 ls.current_faculty, ls.add_inf, ls.object_type, ls.flow_id, load.course,
		 sum (CASE 
			WHEN work_type=101  THEN work_count
		    ELSE 0
		 END) AS lec_count,
		sum (CASE 
			WHEN work_type=103 or work_type=104  THEN work_count
		    ELSE 0
		 END) AS pract_count, load.year, subgroups.id as sid, budzhet_count, vnebudzhet_count, budzhet_foreign, vnebudzhet_foreign
		 from loadstrokes as ls, load, lessonplanstroke as lp, lessonplanworks as lw, subgroups
		 where ls.uuid=?
		 and group_id=subgroups.parent_id
		 and subgroups.block_id=?
		 and ls.active=true
		 and group_type='profile'
		 and ls.load_id=load.id 
		 and lp.uuid=ls.uuid and lw.lessonplanstroke_id=lp.id
		 and load.version=?
		 and hours_type in #{stq}
		 group by ls.id, ls.load_id, ls.code, ls.name, ls.sem, ls.budzhet,
		 ls.department,  ls.choice, ls.count_students, ls.count_groups, ls.teacher_id, 
		 ls.group_id, ls.alias_name,  ls.is_own, ls.count_hours, ls.hours_type,
		 ls.current_faculty, ls.add_inf, ls.object_type, ls.flow_id, load.course, load.year, subgroups.id>>)

		 qload.setString(0, uuid)	
		 //qload.setString(1, budzhet)	
		 qload.setInt(1, blockId)
		 qload.setInt(2, loadVersion)

	// 	 //var num: Int! = 0

		 qload.query: {
	 		fix row = $
	 		fix id = row.int(0)
	 		println: id
	 		year = row.int('year).*dyn(#!)
	 		fix type = row.int('object_type)

	 		if (type==2) {
	 			fix uplq = db.prepare(<<UPDATE loadstrokes SET count_hours=0, active=false WHERE id=?;>>)
			 	uplq.setInt(0, id)
			 	uplq.update()
	 		}
	 		else {
	 			fix uplq = db.prepare(<<UPDATE loadstrokes SET active=false WHERE id=?;>>)
			 	uplq.setInt(0, id)
			 	uplq.update()
	 		}
	 		

		 	fix insert = db.prepareKey(<<INSERT INTO loadstrokes(
			 uuid, load_id, code, name, sem, budzhet, department, choice, 
			 count_students, count_groups, teacher_id, group_id, alias_name, 
			 count_hours, hours_type, is_own, active, current_faculty, add_inf, object_type, group_type, flow_id, count_foreign)
			 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);>>)

		 	 fix countGr = row.int('count_groups).*dyn(#!)
		 		 		  
	 		   //num = num + 1

	 		  insert.setString(0, uuid)
			  insert.setInt(1, row.int('load_id))
			  insert.setString(2, row.string('code))
		      insert.setString(3, row.string('name))
		      insert.setInt(4, row.int('sem))
		      insert.setUnit(5, row.boolean('budzhet))
		      fix dep = row.int('department)
		      var sg = subDepartments.where($.subgroupId==row.int('sid))
		      if (sg!=())
		      	insert.setInt(6, sg.[0].department)
		      else
		      	insert.setInt(6, dep)	
		      insert.setUnit(7, row.boolean('choice))  
		      var count: Int? = if (row.boolean('budzhet)==unit) row.int('budzhet_count) else row.int('vnebudzhet_count)
			  
			  if (type==2) {
			  	insert.setInt(8, 0)
		      	insert.setInt(9, 0)
			  }
			  else {
			  	insert.setInt(8, count)
		        insert.setInt(9, countGr)
			  }
			  
		      insert.setInt(10, row.int('teacher_id))
		      insert.setInt(11, row.int('sid))
		      insert.setString(12, row.string('alias_name))
		      insert.setInt(13, row.int('count_hours))
		      insert.setString(14, row.string('hours_type))
		      insert.setUnit(15, row.boolean('is_own))
		      insert.setUnit(16, unit)
		      insert.setUnit(17, row.boolean('current_faculty))
		      insert.setInt(18, row.int('add_inf))
		      insert.setInt(19, type)
		      insert.setString(20, "subgroup")
		      insert.setInt(21, row.int('flow_id))
		      var fcount: Int? = if (row.boolean('budzhet)==unit) row.int('budzhet_foreign) else row.int('vnebudzhet_foreign)
		  //     //println: 
		 	  insert.setInt(22, fcount)
		 	  fix k = insert.updateKey()
		     
		      //strokes += alm/LoadStroke(uuid, row.string('code), row.string('name), row.string('alias_name), row.int('course), row.int('sem), row.boolean('choice), count, countGr, row.int('lec_count), lecture_fact, row.int('pract_count), practice_fact, lab_fact, row.int('add_inf), row.boolean('is_own), unit, row.boolean('current_faculty), sum, row.int('department), unit, cv)  

	 	}


}


def joinGroups(uuid:String!, loadVersion:Int!, groupId: Int?) {
	//var year:Int! = 0

	// db.work(8): {

		var type: Int?
		var count: Int?

		fix sel =  db.prepare(<<select max(count_hours), ls.object_type from loadstrokes as ls where id in (SELECT loadstrokes.id from loadstrokes, load
		where loadstrokes.load_id=load.id and load.version=? and uuid=?
		  and group_type='subgroup' and group_id in (select id from subgroups where parent_id=?))
		 group by ls.object_type>>)

		sel.setString(1, uuid)
		sel.setInt(0, loadVersion)
		sel.setInt(2, groupId)

		sel.query: {
			type = $.int('object_type)
			count = $.int(0)
		}


		fix loqdq = db.prepare(<<delete from loadstrokes where id in (SELECT loadstrokes.id from loadstrokes, load
		where loadstrokes.load_id=load.id and load.version=? and uuid=?
		  and group_type='subgroup' and group_id in (select id from subgroups where parent_id=?));>>)

		loqdq.setString(1, uuid)
		loqdq.setInt(0, loadVersion)
		loqdq.setInt(2, groupId)
		loqdq.update()

		if (type==2) {
			fix uplq = db.prepare(<<UPDATE loadstrokes SET count_hours=? WHERE id in (SELECT loadstrokes.id from loadstrokes, load
			where loadstrokes.load_id=load.id and load.version=? and uuid=?
			and group_type='profile' and group_id=? and count_groups=1);>>)
			uplq.setString(2, uuid)
			uplq.setInt(1, loadVersion)
			uplq.setInt(0, count)
			uplq.setInt(3, groupId)
			uplq.update()

			fix uplq2 = db.prepare(<<UPDATE loadstrokes SET active=true WHERE id in (SELECT loadstrokes.id from loadstrokes, load
			where loadstrokes.load_id=load.id and load.version=? and uuid=?
			and group_type='profile' and group_id=?);>>)
			uplq2.setString(1, uuid)
			uplq2.setInt(0, loadVersion)
			uplq2.setInt(2, groupId)
			uplq2.update()
		}

		else {
			fix uplq = db.prepare(<<UPDATE loadstrokes SET active=true WHERE id in (SELECT loadstrokes.id from loadstrokes, load
			where loadstrokes.load_id=load.id and load.version=? and uuid=?
			 and active=false and group_type='profile' and group_id=?);>>)
			uplq.setString(1, uuid)
			uplq.setInt(0, loadVersion)
			uplq.setInt(2, groupId)
			uplq.update()
		}

		

		

	// }
}




